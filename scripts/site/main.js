var granim = new Granim({
    element: '#bg',
    name: 'basic-gradient',
    direction: 'left-right',
    opacity: [1, 1],
    isPausedWhenNotInView: true,
    states : {
        "default-state": {
            gradients: [
                ['#fb8383', '#d43737'],
                ['#a67de5', '#543288'],
            ]
        }
    }
});